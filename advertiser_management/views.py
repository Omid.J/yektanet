from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import CreateView, ListView, RedirectView, TemplateView
from django.db.models import Count, Func, Avg, Max, F, Q, ExpressionWrapper, DurationField, FloatField
from django.db.models import Subquery, OuterRef, fields

from django.db.models.functions import ExtractHour, Coalesce

from .models import *
from .forms import CreateAdForm

import datetime

class AdvertiserListView(ListView):
	model = Advertiser
	template_name = "advertiser_management/ads.html"
	context_object_name = 'advertisers'
	def get_queryset(self):
		return super().get_queryset()

	def create_views(self, advertisers):
		for advertiser in advertisers:
			for ad in advertiser.approved_adds:
				view = View(ad=ad, date=timezone.now(), ip_address=self.request.ip_address)
				view.save()

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		advertisers = context['advertisers']
		for advertiser in advertisers:
			advertiser.approved_adds = advertiser.ad_set.filter(approved=True)
		self.create_views(advertisers)
		return context

class AdClickRedirectView(RedirectView):
	permanent = True

	def get_redirect_url(self, *args, **kwargs):
		ad = get_object_or_404(Ad, pk=kwargs['ad_id'])
		self.create_click(ad)
		return ad.link

	def create_click(self, ad):
		click = Click(ad=ad, date=timezone.now(), ip_address=self.request.ip_address)
		click.save()

class AdCreateView(CreateView):
	model = Ad
	form_class = CreateAdForm
	def get_success_url(self):
		return reverse('advertiser_management:index')

class AdReportView(TemplateView):
	template_name = 'advertiser_management/report.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)

		context['total'] = Ad.objects.annotate(
			click_count=Count('click', distinct = True),
			view_count=Count('view', distinct = True),
			click_rate=100 * Count('click', distinct = True) / Count('view', distinct = True),
		).order_by('-click_rate')
		
		context['hourly'] = Ad.objects.annotate(
			hour=ExtractHour('view__date'),
			click_count=Count('click', filter=Q(click__date__hour=ExtractHour('view__date')), distinct = True),
			view_count=Count('view', distinct=True),
			click_rate=100 * Count('click', filter=Q(click__date__hour=ExtractHour('view__date')), distinct=True) / Count('view', distinct=True),
		).order_by('id', 'hour')

		ads = Ad.objects.all()
		for ad_itr in ads:
			time_sum = 0
			cnt = 0
			for click in Click.objects.filter(ad=ad_itr).all():
				view_time = None
				for view in View.objects.filter(ad=ad_itr, ip_address=click.ip_address).annotate():
					if view.date < click.date and (not view_time or view_time < view.date):
						view_time = view.date
				if view_time:
					cnt += 1
					time_sum += (click.date - view_time).total_seconds()
			ad_itr.avg_time = (time_sum / cnt) if cnt > 0 else None
		context['diffs'] = ads

		return context

		'''ads = Ad.objects.filter(click__isnull=False, view__isnull=False).annotate(
			avg_time=Avg(
				Coalesce(Func(F('click__date'), function='strftime', template='%s'), 0) - Coalesce(
					Subquery(
						View.objects.filter(
							ad=OuterRef('pk'),
							ip_address=OuterRef('click__ip_address'),
							date__lt=OuterRef('click__date')
						).order_by('-date')[:1].values('date').annotate(
							unix_timestamp=Func(F('date'), function='strftime', template='%s')
						).values('unix_timestamp')
					), 0
				), output_field=FloatField()
			)
		)
		context['diffs'] = ads

		return context'''


