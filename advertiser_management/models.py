from django.db import models, transaction

class Advertiser(models.Model):
	name = models.CharField(max_length=200)
	def __str__(self):
		return self.name

class Ad(models.Model):
	advertiser = models.ForeignKey(Advertiser, on_delete=models.CASCADE)
	title = models.CharField(max_length=200)
	img_url = models.URLField(max_length=200)
	link = models.URLField(max_length=200)
	approved = models.BooleanField(default=False)
	def __str__(self):
		return self.title

class Click(models.Model):
	ad = models.ForeignKey(Ad, on_delete=models.CASCADE)
	date = models.DateTimeField('date clicked')
	ip_address = models. GenericIPAddressField()
	def __str__(self):
		return self.ad.title

class View(models.Model):
	ad = models.ForeignKey(Ad, on_delete=models.CASCADE)
	date = models.DateTimeField('date viewed')
	ip_address = models. GenericIPAddressField()
	def __str__(self):
		return self.ad.title