from django import forms

from .models import *

class CreateAdForm(forms.ModelForm):

	advertiser = forms.ModelChoiceField(
		queryset=Advertiser.objects.all(), 
		widget=forms.Select(
			attrs={'class': 'form-control'}
	))

	img_url = forms.URLField(
		widget=forms.TextInput(
			attrs={'placeholder': 'Image URL', 'class': 'form-control'}
	))

	title = forms.CharField(
		widget=forms.TextInput(
			attrs={'placeholder': 'Title', 'class': 'form-control'}
	))

	link = forms.URLField(
		widget=forms.TextInput(
			attrs={'placeholder': 'Link URL', 'class': 'form-control'}
	))
	class Meta:
		model = Ad
		fields = ['advertiser', 'img_url', 'title', 'link']