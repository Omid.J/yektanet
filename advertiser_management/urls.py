from django.urls import path

from . import views

app_name = 'advertiser_management'
urlpatterns = [
    path('', views.AdvertiserListView.as_view(), name='index'),
    path('report/', views.AdReportView.as_view(), name='report'),
    path('create/', views.AdCreateView.as_view(), name='create'),
    path('click/<int:ad_id>/', views.AdClickRedirectView.as_view(), name='ad_click'),
]
