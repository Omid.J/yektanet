from django.contrib import admin

from .models import *

admin.site.register(Advertiser)
admin.site.register(Click)
admin.site.register(View)

@admin.register(Ad)
class AdAdmin(admin.ModelAdmin):
	list_display = ['title', 'advertiser', 'approved']
	list_filter = ('approved',)
	search_fields = ('title',)